# GitLu

GitLu is a Git server for an Apache server. The special feature of GitLu is that ~~Git doesn't must installed on the server~~.
GitLu runs on a normal Apache server side by side with your website. So it is not required to open a further port or to run a further server.

## Required
 * customize of the .htaccess file
 * Apache server
 * PHP version >= 7.1 (because of types and void)
 * MySQL database
 
## NOT Required
 * full access on the server
 * ~~Git installation on the server~~
 
 
## ToDo
 * [x] `fetch` (dumb & smart)
 * [x] `push`
 * [ ] frontend
 * [ ] 100% documentation
 * [ ] branch or fork for PHP < 7.1
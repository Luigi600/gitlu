<?php


namespace GitLu;


use http\Exception\InvalidArgumentException;

/**
 * Static class that contains generally functions and constants.
 * @package GitLu
 */
final class Functions {
    /** @var string The regex pattern of all valid characters for an object folder and file. */
    const POSSIBLE_CHARS = "[A-Za-z0-9]";

    /**
     * Checks whether a query is included in the URL.
     * Example:
     * inQuery("/test.php?abc=123&query2=5",   array( "abc" => "123"   ,   "query2" => "5") );
     * @remarks All parameters are case sensitive.
     * @param string $url The URL to be check.
     * @param array $queryChecker The queries to be a part of the URL (name AND value).
     * @return bool Returns true, when ALL queries are found in URL. Otherwise false.
     */
    static function inQuery(string $url, array $queryChecker): bool
    {
        $parsedURL = parse_url($url);
        if(!isset($parsedURL["query"])) return false; // if the url doesn't contain query part

        parse_str($parsedURL["query"], $query);

        $successfulChecker = 0;
        foreach($queryChecker as $queryKey => $queryVal) {
            if(isset($query[$queryKey]) && $query[$queryKey] == $queryVal)
                $successfulChecker++;
        }

        return ($successfulChecker == count($queryChecker));
    }

    /**
     * Gets all subdirectories of a directory.
     * @remarks The return is a list of all subdirectories than FULL path.
     * @remarks All parameters are case sensitive.
     * @param string $fullPath The full path to the directory to be search.
     * @param array $excludedNames The directory names to be exclude.
     * @throws \InvalidArgumentException Throws InvalidArgumentException when the fullPath does not exist.
     * @return array Returns a list of subdirectories.
     */
    static function getDirectories(string $fullPath, array $excludedNames = []): array
    {
        if(!file_exists($fullPath))
            throw new InvalidArgumentException("Path to directory doesn't exist");

        $result = [];
        foreach(scandir($fullPath) as $dir) {
            if(in_array($dir, $excludedNames))
                continue;

            $fullPathToDir = $fullPath."/".$dir;
            if(is_dir($fullPathToDir))
                array_push($result, $fullPathToDir);
        }

        return $result;
    }

    /**
     * Gets a list of files of a directory.
     * @remarks The return is a list of all files than FULL path.
     * @remarks All parameters are case sensitive.
     * @param string $fullPath The full path to the directory to be search.
     * @param array $excludedNames The file names to be exclude.
     * @param array $extensions The file extensions to be filter. When the length of the array is 0, there are no filter.
     * @throws \InvalidArgumentException Throws InvalidArgumentException when the fullPath does not exist.
     * @return array Returns a list of files.
     */
    static function getFiles(string $fullPath, array $excludedNames = [], array $extensions = []): array
    {
        if(!file_exists($fullPath))
            throw new InvalidArgumentException("Path to directory doesn't exist");

        $result = [];
        foreach(scandir($fullPath) as $file) {
            if(in_array($file, $excludedNames))
                continue;

            $fullPathToFile = $fullPath."/".$file;
            if(is_file($fullPathToFile)) {
                // check extension
                if(count($extensions) > 0) {
                    $pathInfo = pathinfo($fullPathToFile);
                    if(!in_array(strtolower($pathInfo["extension"]), $extensions))
                        continue;
                }

                array_push($result, $fullPathToFile);
            }
        }

        return $result;
    }

    /**
     * Gets the direct path to the ".git" directory of a repository path.
     * @param $repoDir
     * @return string Returns the path to "REPO/.git/".
     */
    static function getDirectPathToGitDirectory($repoDir): string
    {
        if(preg_match('/(\\|\/)$/', $repoDir))
            return $repoDir.".git/";

        return $repoDir."/.git/";
    }
}
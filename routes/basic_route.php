<?php
/*
 * TODO:
 *   - protected members: access about getters
 */


namespace GitLu\Routes;


use GitLu\Headers\RequestObject;
use GitLu\Headers\ResponseHeader;

/**
 * The basic class of a route.
 * @package GitLu\Routes
 */
abstract class BasicRoute
{
    /** @var ResponseHeader The response header to be return. */
    protected $responseHeader;

    /** @var string The HTTP method of this route. */
    protected $method;

    public function __construct(string $method)
    {
        $this->responseHeader = new ResponseHeader();
        $this->method         = strtolower($method);
    }

    //region Getters
    //endregion

    /**
     * Gets the response header of this route. The response header is not completed at this time. It will "build" currently.
     * @return ResponseHeader Returns the incomplete response header.
     */
    protected function getHeader(): ResponseHeader { return $this->responseHeader; }

    /**
     * Gets the HTTP method of this route.
     * @return string Returns the HTTP method.
     */
    protected function getHTTPMethod(): string { return $this->method; }

    /**
     * Gets the completed response header. This function will call when `match` is successful.
     * @param RequestObject $req The HTTP request wrapped into a class.
     * @return ResponseHeader Returns a completed response header.
     */
    public abstract function getResponseHeader(RequestObject $req): ResponseHeader;

    /**
     * Checks whether the request equals the route conditions.
     * @param RequestObject $req The HTTP request wrapped into a class.
     * @return bool Returns true whether the request match. Otherwise false.
     */
    public function match(RequestObject $req): bool {
        return ($this->method == strtolower($req->getMethod()));
    }
}
<?php


namespace GitLu\Routes\Dumb;


use GitLu\Functions;
use GitLu\Headers\RequestObject;

class FileByURL extends Route
{
    public function __construct(string $matchCond, bool $isRegex = false)
    {
        parent::__construct($matchCond, $isRegex);
    }

    protected function setHeader(RequestObject $req): void
    {
        // combines REPO/.git/URL because URL shows on the right path on the filesystem
        $this->readFileAndSetHeaders(Functions::getDirectPathToGitDirectory($req->getRepositoryDirectory()).$req->getMainURL());
    }
}
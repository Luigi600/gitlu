<?php


namespace GitLu\Routes\Dumb;

/*
 * (NE = NO EXCEPTION what means no 404 status code or exceptions)
 */
abstract class FileByURL_NE extends FileByURL
{
    public function __construct(string $matchCond, bool $isRegex = false)
    {
        parent::__construct($matchCond, $isRegex);
    }

    protected function readFileAndSetHeaders(string $fullPath): void
    {
        try {
            $this->readFile($fullPath);
        }
        catch (\Exception $exception) {
            $this->responseHeader->setStatus(200);
            $this->responseHeader->setContent(""); // returns an empty file - NO 404 STATUS !!!
        }
    }
}
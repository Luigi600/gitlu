<?php


namespace GitLu\Routes\Dumb;


use GitLu\Functions;
use GitLu\Headers\RequestObject;

class InfoReferences extends Route
{
    public function __construct()
    {
        parent::__construct("/info/refs");
    }

    protected function setHeader(RequestObject $req): void
    {
        $result = [];

        // combines .git-directory and refs
        $gitDir = Functions::getDirectPathToGitDirectory($req->getRepositoryDirectory()); // REPO/.git/
        $refDir = $gitDir."refs";

        // repo file exists (because otherwise match and this function doesn't call)
        if(file_exists($refDir)) {
            // go to in each folder in the refs folder
            foreach(Functions::getDirectories($refDir, [".", ".."]) as $dir) {
                // read file content and file name (than result)
                foreach(Functions::getFiles($dir) as $file) {
                    array_push($result, trim(file_get_contents($file))."\t".substr($file, strlen($gitDir)));
                }
            }
        }

        $this->responseHeader->setContent(join("\n", $result)."\n"); // repo exists to 100%, so http code is 200
    }
}
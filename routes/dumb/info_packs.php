<?php
/**
 * TODO:
 *   - Check up the structure of a "/info/packs" file in complex git projects
 */


namespace GitLu\Routes\Dumb;


use GitLu\Functions;
use GitLu\Headers\RequestObject;

class InfoPacks extends Route
{
    public function __construct()
    {
        parent::__construct("/objects/info/packs");
    }

    /**
     * ...
     * The function ignores the "/objects/info/packs" file because the file must be created by "update-server-info" commando.
     * It generates the information of the file by itself.
     * @param RequestObject $req
     */
    protected function setHeader(RequestObject $req): void
    {
        $result = [];
        $tmp    = [];

        // combines .git-directory and "objects/pack"
        $packDir = Functions::getDirectPathToGitDirectory($req->getRepositoryDirectory())."objects/pack";

        // checks if there exists a pack AND idx file in the pack dir
        // when true: add filename to result
        foreach(Functions::getFiles($packDir, [], ["pack", "idx"]) as $file) {
            $pathInfo = pathinfo($file);
            $fileName = $pathInfo["filename"];
            if(in_array($fileName, $tmp))
                array_push($result, "P ".$fileName.".pack");
            else
                array_push($tmp, $fileName);
        }

        $this->responseHeader->setContent(join("\n", $result)); // returns an empty file - NO 404 STATUS !!!
    }
}
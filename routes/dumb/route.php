<?php
/**
 * TODO:
 *   - refactoring:
 *       - move function readFile outside
 */


namespace GitLu\Routes\Dumb;


use GitLu\Headers\RequestObject;
use GitLu\Headers\ResponseHeader;
use GitLu\Routes\BasicRoute;

abstract class Route extends BasicRoute
{
    private $matchCond;
    private $isRegex;

    public function __construct(string $matchCond, bool $isRegex = false)
    {
        parent::__construct("GET");
        $this->matchCond      = $matchCond;
        $this->isRegex        = $isRegex;
    }

    public function getResponseHeader(RequestObject $req): ResponseHeader
    {
        $this->setHeader($req);
        return $this->responseHeader;
    }

    public function match(RequestObject $req): bool
    {
        if(!parent::match($req)) return false;

        if($this->isRegex) return preg_match($this->matchCond, $req->getMainURL());

        return ($req->getMainURL() == $this->matchCond);
    }

    protected abstract function setHeader(RequestObject $req): void;

    protected function readFile(string $fullPath): void
    {
        $filePointer = fopen($fullPath,'rb');
        if(!$filePointer) throw new \Exception();

        $stat = fstat($filePointer);

        $this->responseHeader->setLastModifiedDate($stat['mtime']);
        $this->responseHeader->setContent(fread($filePointer, filesize($fullPath)));
        fclose($filePointer);
    }

    protected function readFileAndSetHeaders(string $fullPath): void
    {
        try {
            $this->readFile($fullPath);
        }
        catch (\Exception $exception) {
            $this->responseHeader->setStatus(404);
            $this->responseHeader->setContent("File not found!");
        }
    }
}
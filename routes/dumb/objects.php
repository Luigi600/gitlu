<?php


namespace GitLu\Routes\Dumb;


use GitLu\Functions;

class Objects extends FileByURL
{
    public function __construct()
    {
        parent::__construct('/\/objects\/' . Functions::POSSIBLE_CHARS . '{2}\/' . Functions::POSSIBLE_CHARS . '+/', true);
        $this->responseHeader->setContentType("git-loose");
    }
}
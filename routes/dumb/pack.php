<?php


namespace GitLu\Routes\Dumb;


use GitLu\Functions;

class Pack extends FileByURL
{
    public function __construct()
    {
        parent::__construct('/\/objects\/pack\/pack-' . Functions::POSSIBLE_CHARS . '+\.(idx|pack)/', true);
    }

    protected function readFileAndSetHeaders(string $fullPath): void {
        try {
            $this->readFile($fullPath);

            $pathInfo = pathinfo($fullPath);
            if(strtolower($pathInfo["extension"]) == "idx")
                $this->responseHeader->setContentType("git-idx");
            elseif(strtolower($pathInfo["extension"]) == "pack")
                $this->responseHeader->setContentType("git-pack");
        }
        catch (\Exception $exception) {
            $this->responseHeader->setStatus(404);
            $this->responseHeader->setContent("File not found!");
        }
    }
}
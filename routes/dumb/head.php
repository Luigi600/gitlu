<?php


namespace GitLu\Routes\Dumb;


use GitLu\Functions;
use GitLu\Headers\RequestObject;

class Head extends Route
{
    public function __construct()
    {
        parent::__construct("/HEAD");
    }

    protected function setHeader(RequestObject $req): void
    {
        $headFile = Functions::getDirectPathToGitDirectory($req->getRepositoryDirectory())."HEAD";
        $this->readFileAndSetHeaders($headFile);
    }
}
<?php


namespace GitLu\Routes\Smart;


class ReceivePack extends ServiceGET
{
    public function __construct()
    {
        parent::__construct("git-receive-pack", "001f");
    }
}
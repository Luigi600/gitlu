<?php


namespace GitLu\Routes\Smart;


class UploadPackPOST extends ServicePOST
{
    public function __construct()
    {
        parent::__construct("git-upload-pack");
    }
}
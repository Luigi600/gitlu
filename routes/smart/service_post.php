<?php


namespace GitLu\Routes\Smart;


use GitLu\Configurations\Configuration;
use GitLu\Headers\RequestObject;
use GitLu\Headers\ResponseHeader;
use GitLu\Routes\BasicRoute;

class ServicePOST extends BasicRoute
{
    private $service;

    public function __construct(string $service)
    {
        parent::__construct("POST");

        $this->service = $service;
    }

    public function match(RequestObject $req): bool
    {
        if(!parent::match($req)) return false;

        return ($req->getMainURL() == "/".$this->service);
    }

    public function getResponseHeader(RequestObject $req): ResponseHeader
    {
        $descriptorSpec = array(
            0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        );

        if(Configuration::ENV_MODE == "debug")
            $descriptorSpec[2] = array("file", Configuration::GIT_SERVICE_ERROR_FILE, "a"); // stderr is a file to write to;

        // windows...
        // https://stackoverflow.com/a/36494732
        $cmd = sprintf('"%s%s" --stateless-rpc "%s"', Configuration::GIT_SERVICE_BIN, $this->service . Configuration::EXTENSION_OS, escapeshellarg($req->getRepositoryDirectory()));
        if (strtolower(substr(PHP_OS, 0, 3)) === 'win')
            $cmd = '"'.$cmd.'"';

        $process = proc_open($cmd, $descriptorSpec, $pipes);
        if(is_resource($process)) {
            try {
                fwrite($pipes[0], file_get_contents("php://input")); // write POST data
                fclose($pipes[0]);

                $this->responseHeader->setContent(stream_get_contents($pipes[1]));
                $this->responseHeader->setContentType($this->service.'-result');
                fclose($pipes[1]);
            } catch (\Exception $exception) {
                print_r($exception);
                // ...
            } finally {
                $code = proc_close($process);
                //if($this->responseHeader->isContentEmpty())
                //echo $code;
            }
        }

        return $this->responseHeader;
    }
}
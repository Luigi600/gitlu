<?php


namespace GitLu\Routes\Smart;


class UploadPackGET extends ServiceGET
{
    public function __construct()
    {
        parent::__construct("git-upload-pack", "001e");
    }
}
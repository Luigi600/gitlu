<?php


namespace GitLu\Routes\Smart;


class ReceivePackPOST extends ServicePOST
{
    public function __construct()
    {
        parent::__construct("git-receive-pack");
    }
}
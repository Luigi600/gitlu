<?php


namespace GitLu\Routes\Smart;


use GitLu\Configurations\Configuration;
use GitLu\Functions;
use GitLu\Headers\RequestObject;
use GitLu\Headers\ResponseHeader;
use GitLu\Routes\BasicRoute;

class ServiceGET extends BasicRoute
{
    private $service;

    private $version;

    public function __construct(string $service, string $version)
    {
        parent::__construct("GET");

        $this->service = $service;
        $this->version = $version;
    }

    public function match(RequestObject $req): bool
    {
        if(!parent::match($req)) return false;

        if($req->getMainURL() != "/info/refs") return false;

        return Functions::inQuery($req->getURL(), array(
            "service" => $this->service
        ));
    }

    public function getResponseHeader(RequestObject $req): ResponseHeader
    {
        $cmd = sprintf('"%s%s" "%s"', Configuration::GIT_SERVICE_BIN, $this->service . Configuration::EXTENSION_OS, $req->getRepositoryDirectory());

        exec($cmd, $output, $exitCode);
        if(!preg_match('/[0-9a-z]{48}/', $output[0])) // normal length 44, +4 for "0000"
            $output[0] = "0000".$output[0]; // NONE "\n", so just put to the first item

        array_unshift($output, $this->version.'# service='.$this->service); // add service and "version" (TODO: git client check the version?!)

        $this->responseHeader->setContentType($this->service);
        $this->responseHeader->setContent(join("\n", $output));
        return $this->responseHeader;
    }
}
<?php


namespace GitLu\Headers;


/**
 * Represents a HTTP request.
 * @package GitLu\Headers
 */
final class RequestObject
{
    /** @var string The HTTP request method. */
    private $method;

    /** @var string The full request URL. */
    private $url;

    /** @var string The path to the repository of this request. */
    private $repositoryDirectory;

    public function __construct(string $method, string $url, string $repoDir)
    {
        $this->method = $method;
        $this->url    = $url;
        $this->repositoryDirectory = $repoDir;
    }

    //region Getters
    /**
     * Gets the full request URL.
     * @return string Returns the full request URL.
     */
    public function getURL(): string
    {
        return $this->url;
    }

    /**
     * Gets the request URL without query and fragment part.
     * @return string Returns the request URL without parameters.
     */
    public function getMainURL(): string
    {
        return strtok($this->url, '?');
    }

    /**
     * Gets the HTTP request method.
     * @return string Returns the HTTP request method than string.
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Gets the repository path.
     * @return string Returns the repository path.
     */
    public function getRepositoryDirectory(): string
    {
        return $this->repositoryDirectory;
    }
    //endregion
}
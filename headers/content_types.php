<?php


namespace GitLu\Headers;


/**
 * Static class that contains possible HTTP content types. This class has the function than enum.
 * @package GitLu\Headers
 */
final class ContentType
{
    /** @var string[] The types of all possible HTTP content types. */
    const TYPES = [
        "plain"                    => "text/plain; charset=utf-8",
        "git-loose"                => "application/x-git-loose-object",
        "git-pack"                 => "application/x-git-packed-objects",
        "git-idx"                  => "application/x-git-packed-objects-toc",
        "git-receive-pack"         => "application/x-git-receive-pack-advertisement",
        "git-upload-pack"          => "application/x-git-upload-pack-advertisement",
        "git-upload-pack-result"   => "application/x-git-upload-pack-result",
        "git-receive-pack-result"  => "application/x-git-receive-pack-result",
    ];
}
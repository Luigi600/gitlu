<?php
/**
 * TODO:
 *   - implementation setter-functions
 *   - implementation getters
 */


namespace GitLu\Headers;


/**
 * Represents a HTTP response header.
 * @package GitLu\Headers
 */
final class ResponseHeader
{
    /** @var int The HTTP status code of the response. */
    private $status;

    /**
     * @var bool Specifics whether the header to be contain "Cache: no-cache".
     */
    private $setNoCacheHeader;

    /**
     * @var bool Specifics whether the header to be contain a faraway expire date.
     */
    private $setCacheForEver;

    /** @var string The HTTP content of the response. */
    private $content;

    /** @var string The HTTP content type of the response. */
    private $contentType;

    /** @var int The last modified date of the header. */
    private $lastModified;

    public function __construct() {
        $this->status           = 200; // TODO: what is the best default value? 404?
        $this->setNoCacheHeader = false;
        $this->setCacheForEver  = false;
        $this->contentType      = "plain";
        $this->lastModified     = time();
    }

    //region Setters

    /**
     * Sets the HTTP code.
     * @param int $status The HTTP code than int.
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * Sets the content to be return.
     * @param string $content The content.
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function setContentType(string $contentType): void
    {
        // check in_array
        $this->contentType = $contentType;
    }

    public function setLastModifiedDate(int $time): void
    {
        $this->lastModified = $time;
    }

    public function setNoCacheHeader(bool $val): void
    {
        $this->setNoCacheHeader = $val;
    }

    public function setCacheForEver(bool $val): void
    {
        $this->setCacheForEver = $val;
    }
    //endregion

    /**
     * Sets the headers. This function calls direct the PHP "header" functions!
     */
    public function setHeaders() {
        if($this->setNoCacheHeader)
            $this->noCacheHeader();
        elseif($this->setCacheForEver)
            $this->cacheForEver();


        http_response_code($this->status);
        header('Content-Type: '.ContentType::TYPES[$this->contentType]);
        header('Last-Modified: '.date('r', $this->lastModified));
        echo $this->content;
    }

    /**
     *
     */
    private function noCacheHeader() {
        header('Expires: Fri, 01 Jan 1980 00:00:00 GMT');
        header('Pragma: no-cache');
        header('Cache-Control: no-cache, max-age=0, must-revalidate');
    }

    private function cacheForEver() {
        header('Expires: '.date('r', time() + 31536000));
        header('Cache-Control: public, max-age=31536000');
    }
}
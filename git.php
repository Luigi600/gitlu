<?php
/* JUST DEBUG FOR ME IN THE MOMENT */

/* general functions & classes */
require __DIR__ . "/config/config.php";
require __DIR__ . "/functions.php";
require __DIR__ . "/headers/content_types.php";
require __DIR__ . "/headers/response_header.php";
require __DIR__ . "/headers/request_object.php";

/* abstract classes */
require __DIR__ . "/routes/basic_route.php";
require __DIR__ . "/routes/dumb/route.php";

/* routes - dumb */
require __DIR__ . "/routes/dumb/file_by_url.php";
require __DIR__ . "/routes/dumb/file_by_url_ne.php";

require __DIR__ . "/routes/dumb/info_refs.php";
require __DIR__ . "/routes/dumb/head.php";
require __DIR__ . "/routes/dumb/objects.php";
require __DIR__ . "/routes/dumb/info_http_alternates.php";
require __DIR__ . "/routes/dumb/info_packs.php";
require __DIR__ . "/routes/dumb/pack.php";

/* routes - smart */
require __DIR__ . "/routes/smart/service_get.php";
require __DIR__ . "/routes/smart/service_post.php";
require __DIR__ . "/routes/smart/service_receive_pack_get.php";
require __DIR__ . "/routes/smart/service_receive_pack_post.php";
require __DIR__ . "/routes/smart/service_upload_pack_get.php";
require __DIR__ . "/routes/smart/service_upload_pack_post.php";


use GitLu\Headers\RequestObject as Req;
use GitLu\Routes\Dumb as DRoutes;
use GitLu\Routes\Smart as SRoutes;

$repoDir = ".test/test";

$routes = [
    new SRoutes\ReceivePack(),
    new SRoutes\ReceivePackPOST(),
    new SRoutes\UploadPackGET(),
    new SRoutes\UploadPackPOST(),
    new DRoutes\InfoReferences(),
    new DRoutes\Head(),
    new DRoutes\Objects(),
    new DRoutes\HTTPAlternates(),
    new DRoutes\InfoPacks(),
    new DRoutes\Pack()
];

/*
if($url == "/git-upload-pack") {
    print_r($_POST);
    print_r($_REQUEST);
    print_r($_SERVER);
    print_r($_FILES);

    foreach (getallheaders() as $name => $value) {
        echo "$name: $value\n";
    }

    echo file_get_contents("php://input");
}
*/

$req = new Req($_SERVER["REQUEST_METHOD"], $_SERVER["REQUEST_URI"], $repoDir);

foreach($routes as $route) {
    if(!$route->match($req))
        continue;

    $obj = $route->getResponseHeader($req);
    $obj->setHeaders();
    // print_r($obj);
    die();
}
echo "NO_MATCH";
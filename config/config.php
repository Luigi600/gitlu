<?php
/*
 * TODO:
 *   - add db data
 */


namespace GitLu\Configurations;


final class Configuration {
    const ENV_MODE                              = "debug"; // prod or debug
    const GIT_SERVICE_ERROR_FILE                = "logs/service_error.txt"; // just in use when ENV_MODE == "debug"
    const GIT_SERVICE_BIN                       = 'C:\\Program Files\\Git\\mingw64\\bin\\'; // MUST END WITH A PATH CHAR SO \ OR /
    const EXTENSION_OS                          = ".exe";
}